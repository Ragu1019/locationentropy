
from pandas import DataFrame
import pandas as pd
import numpy as np 
import matplotlib.pyplot as plt


def uniquecounts(rows):
        results={}
        for row in rows:
            r=row[len(row)-1]
            if r not in results: results[r]=0
            results[r]+=1
        return results

def entropy(rows):
    from math import log
    log2 = lambda x:log(x)/log(2)
    results=uniquecounts(rows)
    # Now calculate the entropy
    ent=0.0
    for r in results.keys():
        p=float(results[r])/len(rows)
        ent=ent-p*log2(p)
    return ent


data = pd.read_csv('Workbook2.csv')

listcolumn = []
for column in data:
    listcolumn.append(data[column])
    
len(listcolumn)
index = 0
entropy_list = []
for indexcol in  listcolumn:
    if index == 0:
        index+=1
    else:
        entropy_list.append(entropy(indexcol))
months_list = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC']
dict_mon_entropy = {}
for key,value in zip(months_list,entropy_list):
    dict_mon_entropy[key] = value
dict_mon_entropy
entro_df = pd.DataFrame.from_dict(dict_mon_entropy,orient='index')


entro_df.plot(rot=0)
plt.show()








    

